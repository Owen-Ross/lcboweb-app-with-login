package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "rossow1234" ) );
	}
	
	@Test
	public void testIsValidLoginExceptionEmpty( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "" ) );
	}
	
	@Test
	public void testIsValidLoginException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "ro" ) );
	}
	
	@Test
	public void testIsValidLoginExceptionSpecialChars( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "!@#$%^&*" ) );
	}
	
	@Test
	public void testIsValidLoginBoundryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "rossow" ) );
	}
	
	@Test
	public void testIsValidLoginBoundryInOnlyNumbers( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "123456" ) );
	}
	
	@Test
	public void testIsValidLoginBoundryInOnlyLetters( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "abcdef" ) );
	}
	
	@Test
	public void testIsValidLoginBoundryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "rosso" ) );
	}

}
