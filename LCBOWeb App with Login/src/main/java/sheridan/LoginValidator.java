package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
		boolean valid = false;
		
		if(loginName.length() >= 6 && loginName.matches("^[a-zA-Z0-9]+$") && (loginName != null) && !(loginName.matches(""))) {
			valid = true;
		} else {
			valid = false;
		}
		
		return valid;
	}
}
